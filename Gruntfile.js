module.exports = function(grunt) {

  grunt.initConfig({
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'public/css/style.css': 'style.scss'
        }
      }
    },
    pug: {
      compile: {
        options: {
          data: {
            debug: false
          }
        },
        files: {
          'public/index.html': ['index.pug'],
        }
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-pug');
  
  grunt.registerTask('default', ['sass', 'pug']);
  // grunt.registerTask('default', ['jshint']);

};
